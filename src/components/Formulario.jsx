import { useState, useEffect } from 'react'

function Formulario({ pacientes, setPacientes }) {
  //Hooks al inicio de cada componente
  //Hooks no pueden ir dentro de una condicional 
  //No pueden ir después de un return 
  //No pueden ir por fuera del componente 
  const [nombre, setNombre] = useState('');
  const [propietario, setPropietario] = useState('');
  const [email, setEmail] = useState('');
  const [fecha, setFecha] = useState('');
  const [sintomas, setSintomas] = useState('');

  const [error, setError] = useState(false)

  const handleSubmit = (e) => {
    e.preventDefault();
    //Validación del formulario 
    if([nombre, propietario, email, fecha, sintomas].includes('')){
      setError(true)
    }else{
      setError(false)
      const objPaciente = {
        nombre, 
        propietario, 
        email, 
        fecha, 
        sintomas
      }
      //Agrega los elementos nuevos al arreglo 
      setPacientes([...pacientes, objPaciente])
      //Limpia las variables del formulario
      setNombre('')
      setPropietario('')
      setEmail('')
      setFecha('')
      setSintomas('')
    }
  }

  return (
    <div className="md:w-1/2 lg:w-2/5 mx-5">
      <h2 className="font-black text-center text-3xl">Seguimiento pacientes</h2>
      <p className="text-lg mt-5 text-center mb-10">
        Añade pacientes y {""}
        <span className="text-indigo-600 font-bold">Administralos</span>
      </p>
      <form
        onSubmit={ handleSubmit }
        action=""
        className="
        bg-white
        shadow-md
        rounded-lg
        py-10
        px-5
        mb-10
      "
      >
        { error && 
          <div className='w-full text-center'>
            <p className='mb-4 p-2 text-white font-bold bg-red-700' >Todos los cambios son obligatorios</p>
          </div> 
        }
        <div className="mb-5">
          <label
            htmlFor="nombre_mascota"
            className="block text-gray-700 uppercase font-bold ml-2"
          >
            Nombre mascota
          </label>
          <input
            id="nombre_mascota"
            type="text"
            placeholder="Nombre de la mascota"
            className="border-2 w-full p-2 mt-2 placeholder-gray-400 rounded-md"
            value={ nombre }
            onChange={ (e)=> setNombre(e.target.value) }
          />
        </div>
        <div className="mb-5">
          <label
            htmlFor="nombre_propietario"
            className="block text-gray-700 uppercase font-bold ml-2"
          >
            Nombre propietario
          </label>
          <input
            id="nombre_propietario"
            type="text"
            placeholder="Nombre del propietario"
            className="border-2 w-full p-2 mt-2 placeholder-gray-400 rounded-md"
            value={ propietario }
            onChange={ (e)=> setPropietario(e.target.value) }
          />
        </div>
        <div className="mb-5">
          <label
            htmlFor="email"
            className="block text-gray-700 uppercase font-bold ml-2"
          >
            Email
          </label>
          <input
            id="email"
            type="email"
            placeholder="Ingresa tu correo"
            className="border-2 w-full p-2 mt-2 placeholder-gray-400 rounded-md"
            value={ email }
            onChange={ (e)=> setEmail(e.target.value) }
          />
        </div>
        <div className="mb-5">
          <label
            htmlFor="alta"
            className="block text-gray-700 uppercase font-bold ml-2"
          >
            Alta
          </label>
          <input
            id="alta"
            type="date"
            className="border-2 w-full p-2 mt-2 placeholder-gray-400 rounded-md"
            value={ fecha }
            onChange={ (e)=> setFecha(e.target.value) }
          />
        </div>
        <div className="mb-5">
          <label
            htmlFor="sintomas"
            className="block text-gray-700 uppercase font-bold ml-2"
          >
            Síntomas
          </label>
          <textarea
            name=""
            id="sintomas"
            placeholder="Describe los síntomas"
            // cols="30"
            // rows="10"
            className="border-2 w-full p-2 mt-2 placeholder-gray-400 rounded-md"
            value={ sintomas }
            onChange={ (e)=> setSintomas(e.target.value) }
          ></textarea>
        </div>
        <input
          type="submit"
          className="bg-indigo-600 w-full p-3 text-white uppercase font-bold hover:bg-indigo-700 cursor-pointer transition-all"
          value="Agregar paciente"
        />
      </form>
    </div>
  );
}

export default Formulario;
