const Paciente = () => {
  return (
    <div className="bg-white mx-5 my-5 shadow-md px-5 py-10 rounded-xl">
      <p className="font-bold mb-3 text-gray-700 uppercase">
        Nombre: {""} <span className="font-normal normal-case">Hook</span>
      </p>
      <p className="font-bold mb-3 text-gray-700 uppercase">
        Propietario: {""}
        <span className="font-normal normal-case">Eduardo Mares</span>
      </p>
      <p className="font-bold mb-3 text-gray-700 uppercase">
        Email: {""}
        <span className="font-normal normal-case">mail@mail.com</span>
      </p>
      <p className="font-bold mb-3 text-gray-700 uppercase">
        Fecha alta: {""}
        <span className="font-normal normal-case">12/12/2022</span>
      </p>
      <p className="font-bold mb-3 text-gray-700 uppercase">
        Síntomas: {""}
        <span className="font-normal normal-case">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum
          cupiditate, ipsum nam libero beatae sapiente perspiciatis, eveniet,
          est consectetur reprehenderit nemo nobis! Reprehenderit ratione
          impedit dolores neque iure nisi corporis.
        </span>
      </p>
    </div>
  );
};

export default Paciente;
